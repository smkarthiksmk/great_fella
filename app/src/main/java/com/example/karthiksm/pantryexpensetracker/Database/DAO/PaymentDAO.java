package com.example.karthiksm.pantryexpensetracker.Database.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.karthiksm.pantryexpensetracker.Database.Tables.ItemsTable;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.PaymentsTable;

import java.util.List;

@Dao
public interface PaymentDAO {

    @Insert
    void insertPayment(PaymentsTable paymentsTable);

    @Query("Select * from payments_table WHERE groupId  LIKE :groupId AND date >=:oneMonthEarlierTimestamp")
    List<PaymentsTable> listPayments(int groupId, long oneMonthEarlierTimestamp);

    @Query("Delete from payments_table WHERE groupId  LIKE :groupId")
    void deletePayments(int groupId);
}
