package com.example.karthiksm.pantryexpensetracker.Adapter;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.karthiksm.pantryexpensetracker.Database.Tables.GroupTable;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.ItemsTable;
import com.example.karthiksm.pantryexpensetracker.Fragments.AddItems;
import com.example.karthiksm.pantryexpensetracker.MainActivity;
import com.example.karthiksm.pantryexpensetracker.R;
import com.example.karthiksm.pantryexpensetracker.Utilities.Utility;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;



public class ViewTransactionAdapter extends RecyclerView.Adapter<ViewTransactionAdapter.ViewHolder> {

    private List<ItemsTable> itemsDataSet;

    public ViewTransactionAdapter(List<ItemsTable> itemsDataSet) {
        this.itemsDataSet = itemsDataSet;
    }

    @Override
    public ViewTransactionAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_transaction_recycler_view_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewTransactionAdapter.ViewHolder holder, int position) {

        holder.date.setText(Utility.getFormattedDate(itemsDataSet.get(position).getDate(),"dd/MM/yyyy"));
        System.out.println(itemsDataSet.get(position).getItemDescription());
        holder.itemDescription.setText(itemsDataSet.get(position).getItemDescription());
        holder.amount.setText("₹ "+String.valueOf(itemsDataSet.get(position).getItemAmount()));
    }

    @Override
    public int getItemCount() {
        return itemsDataSet.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView date;
        public TextView itemDescription;
        public TextView amount;

        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            itemDescription = itemView.findViewById(R.id.itemDescription);
            amount = itemView.findViewById(R.id.amount);
        }
    }
}
