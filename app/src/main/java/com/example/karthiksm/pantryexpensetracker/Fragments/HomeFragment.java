package com.example.karthiksm.pantryexpensetracker.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.karthiksm.pantryexpensetracker.Adapter.HomeAdapter;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.GroupTable;
import com.example.karthiksm.pantryexpensetracker.MainActivity;
import com.example.karthiksm.pantryexpensetracker.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private RecyclerView homeRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private List<GroupTable> userManagementDataset;
    private TextView no_group_placeholder;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        homeRecyclerView = rootView.findViewById(R.id.paymentsRecyclerView);
        no_group_placeholder = rootView.findViewById(R.id.no_group_placeholder);


        userManagementDataset = new ArrayList<GroupTable>();
        userManagementDataset = MainActivity.groupDatabase.groupDAO().listGroups();
        if(userManagementDataset.isEmpty())
        {
            no_group_placeholder.setVisibility(View.VISIBLE);
            homeRecyclerView.setVisibility(View.INVISIBLE);
        }
        else
        {
            layoutManager = new LinearLayoutManager(getActivity().getApplication());
            homeRecyclerView.setHasFixedSize(true);
            adapter = new HomeAdapter(userManagementDataset);
            homeRecyclerView.setLayoutManager(layoutManager);
            homeRecyclerView.setAdapter(adapter);
        }
        return rootView;
    }

}
