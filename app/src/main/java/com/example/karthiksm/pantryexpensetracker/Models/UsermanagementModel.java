package com.example.karthiksm.pantryexpensetracker.Models;

public class UsermanagementModel {

    private String groupName;
    private String groupMembers;
    private float amount;
    private int color;

    public UsermanagementModel()
    {

    }

    public UsermanagementModel(String groupName, String groupMembers, float amount, int color) {
        this.groupName = groupName;
        this.groupMembers = groupMembers;
        this.amount = amount;
        this.color = color;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(String groupMembers) {
        this.groupMembers = groupMembers;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
