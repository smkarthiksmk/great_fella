package com.example.karthiksm.pantryexpensetracker.Database.DatabaseClasses;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.karthiksm.pantryexpensetracker.Database.DAO.PaymentDAO;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.PaymentsTable;

@Database(entities = PaymentsTable.class, version = 1)
public abstract class PaymentDatabase extends RoomDatabase {
    public abstract PaymentDAO paymentDAO();
}

