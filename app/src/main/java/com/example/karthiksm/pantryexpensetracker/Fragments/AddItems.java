package com.example.karthiksm.pantryexpensetracker.Fragments;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.karthiksm.pantryexpensetracker.Constants.ApplicationConstants;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.GroupTable;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.ItemsTable;
import com.example.karthiksm.pantryexpensetracker.MainActivity;
import com.example.karthiksm.pantryexpensetracker.R;
import com.example.karthiksm.pantryexpensetracker.Utilities.Utility;

import java.util.ArrayList;
import java.util.Locale;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddItems extends Fragment {

    private TextView profilePicture;
    private TextView groupName;
    private TextView amount;
    private TextView noData;
    private TextView itemsCount;
    private TextView itemsAmount;
    private FloatingActionButton speechButton;
    private Button enterTransaction;
    private EditText itemDescriptionInput;
    private TagContainerLayout items;

    private int groupId;
    private int itemAmountToDisplay = 0;
    private ArrayList<String> speechResult;
    private String itemQuantity;
    private String itemDescription;
    private String itemAmount;
    private ArrayList<Integer> itemAmountArray;
    private ArrayList<String> itemToDisplay;
    private SpeechRecognizer mSpeechRecognizer;
    private Intent mSpeechRecognizerIntent;


    public AddItems() {

        // Required empty public constructor
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_add_items, container, false);
        System.out.println(getActivity().getApplication().getPackageName());
        profilePicture = rootView.findViewById(R.id.profilePicture);
        groupName = rootView.findViewById(R.id.groupName);
        amount = rootView.findViewById(R.id.amount);
        noData = rootView.findViewById(R.id.noItemsPresent);
        items = rootView.findViewById(R.id.items);
        speechButton = rootView.findViewById(R.id.speechButton);
        enterTransaction = rootView.findViewById(R.id.enterTransaction);
        itemsCount = rootView.findViewById(R.id.itemsCount);
        itemsAmount = rootView.findViewById(R.id.itemsAmount);
        itemDescriptionInput = rootView.findViewById(R.id.itemDescription);
        itemToDisplay = new ArrayList<>();
        itemAmountArray = new ArrayList<>();

        groupId = getArguments().getInt("id");
        profilePicture.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        profilePicture.setBackground(generateShape(profilePicture.getMeasuredWidth(), getArguments().getInt("profileColor")));
        profilePicture.setText(getArguments().getString("profileName"));
        groupName.setText(getArguments().getString("groupName"));
        amount.setText("₹ " + getArguments().getInt("amount"));


        mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(getActivity().getApplication());
        mSpeechRecognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        mSpeechRecognizerIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                getActivity().getApplication().getPackageName());
        mSpeechRecognizer.setRecognitionListener(new RecognitionListener() {
            @Override
            public void onBeginningOfSpeech() {

            }

            @Override
            public void onBufferReceived(byte[] buffer) {

            }

            @Override
            public void onEndOfSpeech() {

            }

            @Override
            public void onError(int error) {

            }

            @Override
            public void onEvent(int eventType, Bundle params) {

            }

            @Override
            public void onPartialResults(Bundle partialResults) {

            }

            @Override
            public void onReadyForSpeech(Bundle params) {

            }

            @Override
            public void onResults(Bundle results) {
                try {
                    ArrayList<String> speechResult = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                    if (!speechResult.isEmpty()) {
                        String[] splitedSpeechResult = speechResult.get(0).split("\\s+");
                        for (int i = 0; i < splitedSpeechResult.length; i++) {
                            System.out.println(splitedSpeechResult[i]);
                        }

                        itemQuantity = splitedSpeechResult[0];
                        itemAmount = splitedSpeechResult[splitedSpeechResult.length - 1];
                        itemAmountArray.add(Integer.parseInt(itemAmount));
                        String tempDescriptionString = "";
                        for (int i = 1; i < splitedSpeechResult.length - 1; i++) {
                            tempDescriptionString = tempDescriptionString + " " + splitedSpeechResult[i];
                        }
                        itemDescription = tempDescriptionString;
                        if (itemAmount != "") {
                            itemToDisplay.add(itemDescription + " (" + itemQuantity + ") - ₹ " + itemAmount);
                            items.setTags(itemToDisplay);
                            noData.setVisibility(View.INVISIBLE);
                        }
                        itemsCount.setText("(" + itemToDisplay.size() + ")");
                        itemAmountToDisplay = itemAmountToDisplay + Integer.parseInt(itemAmount);
                        itemsAmount.setText("₹ " + itemAmountToDisplay);
                        System.out.println(itemAmountArray);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                    Snackbar.make(rootView, ApplicationConstants.somethingWentWrong, Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onRmsChanged(float rmsdB) {

            }
        });

        speechButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        mSpeechRecognizer.stopListening();
                        break;
                    case MotionEvent.ACTION_DOWN:
                        mSpeechRecognizer.startListening(mSpeechRecognizerIntent);
                        itemQuantity = "";
                        itemDescription = "";
                        itemAmount = "";
                        break;
                }
                return false;
            }

        });


        items.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {

            }

            @Override
            public void onTagLongClick(int position, String text) {

            }

            @Override
            public void onTagCrossClick(int position) {
                System.out.println(itemAmountArray.get(position));
                itemAmountToDisplay = itemAmountToDisplay - itemAmountArray.get(position);
                itemToDisplay.remove(position);
                itemAmountArray.remove(position);
                items.setTags(itemToDisplay);
                itemsAmount.setText("₹ " + itemAmountToDisplay);
                itemsCount.setText("(" + itemToDisplay.size() + ")");
                if (itemToDisplay.isEmpty()) {
                    noData.setVisibility(View.VISIBLE);
                }
            }
        });

        enterTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemToDisplay.isEmpty()) {
                    Snackbar.make(rootView, ApplicationConstants.requiredFields, Snackbar.LENGTH_LONG).show();
                } else {
                    try {
                        ItemsTable itemsTable = new ItemsTable();
                        itemsTable.setGroupId(groupId);
                        itemsTable.setDate(Utility.getTimeStamp());
                        itemsTable.setItemAmount(itemAmountToDisplay);
                        String tempJoinString = itemToDisplay.get(0);
                        for (int i = 1; i < itemToDisplay.size(); i++) {
                            tempJoinString = tempJoinString + "\n" + itemToDisplay.get(i);
                        }

                        itemsTable.setItemDescription(tempJoinString);
                        MainActivity.itemDatabase.itemDAO().insertItem(itemsTable);
                        GroupTable groupTable = new GroupTable();
                        groupTable.setId(getArguments().getInt("id"));
                        groupTable.setProfileName(getArguments().getString("profileName"));
                        groupTable.setColor(getArguments().getInt("profileColor"));
                        groupTable.setGroupName(getArguments().getString("groupName"));
                        groupTable.setGroupMembers(getArguments().getString("groupMembers"));
                        groupTable.setLastAccountActivity(Utility.getTimeStamp());
                        int amountToUpdate = itemAmountToDisplay + getArguments().getInt("amount");
                        groupTable.setAmount(amountToUpdate);
                        MainActivity.groupDatabase.groupDAO().updateGroup(groupTable);
                        Snackbar.make(rootView, ApplicationConstants.transactionSuccessful, Snackbar.LENGTH_LONG).show();
                        MainActivity.fragmentManager.beginTransaction().replace(R.id.content, new HomeFragment()).addToBackStack(null).commit();

                    } catch (Exception e) {
                        Snackbar.make(rootView, ApplicationConstants.somethingWentWrong, Snackbar.LENGTH_LONG).show();
                        MainActivity.fragmentManager.beginTransaction().replace(R.id.content, new HomeFragment()).addToBackStack(null).commit();
                    }
                }
            }
        });
        itemDescriptionInput.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                try {
                    if (!hasFocus) {
                        if (!itemDescriptionInput.getText().toString().isEmpty()) {
                            String[] splitedSpeechResult = itemDescriptionInput.getText().toString().split("\\s+");
                            for (int i = 0; i < splitedSpeechResult.length; i++) {
                                System.out.println(splitedSpeechResult[i]);
                            }

                            itemQuantity = splitedSpeechResult[0];
                            itemAmount = splitedSpeechResult[splitedSpeechResult.length - 1];
                            itemAmountArray.add(Integer.parseInt(itemAmount));
                            String tempDescriptionString = "";
                            for (int i = 1; i < splitedSpeechResult.length - 1; i++) {
                                tempDescriptionString = tempDescriptionString + " " + splitedSpeechResult[i];
                            }
                            itemDescription = tempDescriptionString;
                            if (itemAmount != "") {
                                itemToDisplay.add(itemDescription + " (" + itemQuantity + ") - ₹ " + itemAmount);
                                items.setTags(itemToDisplay);
                                noData.setVisibility(View.INVISIBLE);
                            }
                            itemsCount.setText("(" + itemToDisplay.size() + ")");
                            itemAmountToDisplay = itemAmountToDisplay + Integer.parseInt(itemAmount);
                            itemsAmount.setText("₹ " + itemAmountToDisplay);
                            System.out.println(itemAmountArray);
                        }
                    }
                    itemDescriptionInput.setText("");
                } catch (Exception e) {
                    itemDescriptionInput.setText("");
                    System.out.println(e);
                    Snackbar.make(rootView, ApplicationConstants.somethingWentWrong, Snackbar.LENGTH_LONG).show();
                }
            }
        });

        return rootView;
    }

    public GradientDrawable generateShape(int imageWidth, int color) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setColor(color);
        shape.setCornerRadius(imageWidth);
        return shape;
    }
}
