package com.example.karthiksm.pantryexpensetracker.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.karthiksm.pantryexpensetracker.Database.Tables.ItemsTable;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.PaymentsTable;
import com.example.karthiksm.pantryexpensetracker.R;
import com.example.karthiksm.pantryexpensetracker.Utilities.Utility;

import java.util.List;


public class ViewPaymentsLogAdapter extends RecyclerView.Adapter<ViewPaymentsLogAdapter.ViewHolder> {

    private List<PaymentsTable> paymentsDataSet;

    public ViewPaymentsLogAdapter(List<PaymentsTable> paymentsTable) {
        this.paymentsDataSet = paymentsTable;
    }

    @Override
    public ViewPaymentsLogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_payment_logs_recycler_view_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewPaymentsLogAdapter.ViewHolder holder, int position) {

        holder.date.setText(Utility.getFormattedDate(paymentsDataSet.get(position).getDate(),"dd/MM/yyyy"));
        holder.previousCredit.setText("₹ "+String.valueOf(paymentsDataSet.get(position).getOutstandingBalance()));
        holder.amountPaid.setText("₹ "+String.valueOf(paymentsDataSet.get(position).getAmountPaid()));
    }

    @Override
    public int getItemCount() {
        return paymentsDataSet.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView previousCredit;
        public TextView amountPaid;
        public TextView date;

        public ViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date);
            previousCredit = itemView.findViewById(R.id.previousCredit);
            amountPaid = itemView.findViewById(R.id.amountPaid);
        }
    }
}