package com.example.karthiksm.pantryexpensetracker.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.karthiksm.pantryexpensetracker.Adapter.ViewPaymentsLogAdapter;
import com.example.karthiksm.pantryexpensetracker.Adapter.ViewTransactionAdapter;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.ItemsTable;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.PaymentsTable;
import com.example.karthiksm.pantryexpensetracker.MainActivity;
import com.example.karthiksm.pantryexpensetracker.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewPaymentLogsFragment extends Fragment {
    private RecyclerView viewPaymentRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private List<PaymentsTable> paymentsDataSet;
    private TextView no_payment_logs_placeholder;
    private TextView date;
    private TextView previousBalance;
    private TextView amontPaid;


    private int groupId;
    public ViewPaymentLogsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_view_payment_logs, container, false);
        groupId = getArguments().getInt("id");
        viewPaymentRecyclerView = rootView.findViewById(R.id.paymentLogsRecyclerView);
        no_payment_logs_placeholder = rootView.findViewById(R.id.no_payment_placeholder);
        date = rootView.findViewById(R.id.dateHeading);
        previousBalance = rootView.findViewById(R.id.previousBalanceHeading);
        amontPaid = rootView.findViewById(R.id.dateHeading);
        paymentsDataSet = new ArrayList<PaymentsTable>();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        long oneMonthEarlierTimestamp = cal.getTimeInMillis();
        paymentsDataSet = MainActivity.paymentDatabase.paymentDAO().listPayments(groupId, oneMonthEarlierTimestamp);
        if(paymentsDataSet.isEmpty())
        {
            no_payment_logs_placeholder.setVisibility(View.VISIBLE);
            viewPaymentRecyclerView.setVisibility(View.INVISIBLE);
            date.setVisibility(View.INVISIBLE);
            previousBalance.setVisibility(View.INVISIBLE);
            amontPaid.setVisibility(View.INVISIBLE);

        }
        else
        {
            layoutManager = new LinearLayoutManager(getActivity().getApplication());
            viewPaymentRecyclerView.setHasFixedSize(true);
            adapter = new ViewPaymentsLogAdapter(paymentsDataSet);
            viewPaymentRecyclerView.setLayoutManager(layoutManager);
            viewPaymentRecyclerView.setAdapter(adapter);
        }
        return rootView;
    }

}
