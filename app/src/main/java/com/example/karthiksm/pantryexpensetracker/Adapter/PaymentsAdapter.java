package com.example.karthiksm.pantryexpensetracker.Adapter;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.karthiksm.pantryexpensetracker.Database.Tables.GroupTable;
import com.example.karthiksm.pantryexpensetracker.Fragments.MakePaymentsFragment;
import com.example.karthiksm.pantryexpensetracker.MainActivity;
import com.example.karthiksm.pantryexpensetracker.R;
import com.example.karthiksm.pantryexpensetracker.Utilities.Utility;

import java.util.List;



public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.ViewHolder> {

    private List<GroupTable> userManagementDataset;

    public PaymentsAdapter(List<GroupTable> userManagementDataset) {
        this.userManagementDataset = userManagementDataset;
    }

    @Override
    public PaymentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.payment_recycler_view_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PaymentsAdapter.ViewHolder holder, int position) {
        holder.groupProfilePicture.setText(userManagementDataset.get(position).getProfileName());
        holder.groupProfilePicture.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        holder.groupProfilePicture.setBackground(generateShape(holder.groupProfilePicture.getMeasuredWidth(),userManagementDataset.get(position).getColor()));
        holder.groupName.setText(userManagementDataset.get(position).getGroupName());
        holder.amount.setText("₹ "+String.valueOf(userManagementDataset.get(position).getAmount()));
        holder.lastPurchased.setText(Utility.getFormattedDate(userManagementDataset.get(position).getLastAccountActivity(),"dd/MM/yyyy h:mm a"));
    }

    @Override
    public int getItemCount() {
        return userManagementDataset.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView groupProfilePicture;
        public TextView groupName;
        public TextView groupMembers;
        public TextView lastPurchased;
        public TextView amount;

        public ViewHolder(View itemView) {
            super(itemView);
            groupProfilePicture = itemView.findViewById(R.id.groupProfilePicture);
            groupName = itemView.findViewById(R.id.groupName);
            lastPurchased = itemView.findViewById(R.id.lastPurchased);
            amount = itemView.findViewById(R.id.amount);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MakePaymentsFragment makePaymentsFragment =new MakePaymentsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("id",userManagementDataset.get(getLayoutPosition()).getId());
                    bundle.putInt("profileColor",userManagementDataset.get(getLayoutPosition()).getColor());
                    bundle.putString("profileName",userManagementDataset.get(getLayoutPosition()).getProfileName());
                    bundle.putString("groupName",userManagementDataset.get(getLayoutPosition()).getGroupName());
                    bundle.putString("groupMembers",userManagementDataset.get(getLayoutPosition()).getGroupMembers());
                    bundle.putInt("amount",userManagementDataset.get(getLayoutPosition()).getAmount());
                    bundle.putLong("lastAccountActivity",userManagementDataset.get(getLayoutPosition()).getLastAccountActivity());
                    makePaymentsFragment.setArguments(bundle);
                    MainActivity.fragmentManager .beginTransaction().replace(R.id.content, makePaymentsFragment).addToBackStack(null).commit();
                }
            });
        }
    }

    public GradientDrawable generateShape(int imageWidth, int color) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setColor(color);
        shape.setCornerRadius(imageWidth);
        return shape;
    }
}