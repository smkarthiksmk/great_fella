package com.example.karthiksm.pantryexpensetracker.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.karthiksm.pantryexpensetracker.Adapter.PaymentsAdapter;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.GroupTable;
import com.example.karthiksm.pantryexpensetracker.MainActivity;
import com.example.karthiksm.pantryexpensetracker.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentsFragment extends Fragment {

    private RecyclerView paymentsRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private List<GroupTable> userManagementDataset;
    private TextView no_group_placeholder;

    public PaymentsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_payments, container, false);
        paymentsRecyclerView = rootView.findViewById(R.id.paymentsRecyclerView);
        no_group_placeholder = rootView.findViewById(R.id.no_group_placeholder);
        userManagementDataset = new ArrayList<GroupTable>();
        userManagementDataset = MainActivity.groupDatabase.groupDAO().listGroups();
        if (userManagementDataset.isEmpty()) {
            no_group_placeholder.setVisibility(View.VISIBLE);
            paymentsRecyclerView.setVisibility(View.INVISIBLE);
        } else {
            layoutManager = new LinearLayoutManager(getActivity().getApplication());
            paymentsRecyclerView.setHasFixedSize(true);
            adapter = new PaymentsAdapter(userManagementDataset);
            paymentsRecyclerView.setLayoutManager(layoutManager);
            paymentsRecyclerView.setAdapter(adapter);
        }
        return rootView;
    }

}
