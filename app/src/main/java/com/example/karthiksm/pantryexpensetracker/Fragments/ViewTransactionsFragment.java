package com.example.karthiksm.pantryexpensetracker.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.karthiksm.pantryexpensetracker.Adapter.HomeAdapter;
import com.example.karthiksm.pantryexpensetracker.Adapter.ViewTransactionAdapter;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.ItemsTable;
import com.example.karthiksm.pantryexpensetracker.MainActivity;
import com.example.karthiksm.pantryexpensetracker.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewTransactionsFragment extends Fragment {

    private RecyclerView viewTransactionRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private List<ItemsTable> itemsDataSet;
    private TextView no_transaction_placeholder;

    private int groupId;
    public ViewTransactionsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_view_transactions, container, false);
        groupId = getArguments().getInt("id");
        viewTransactionRecyclerView = rootView.findViewById(R.id.transactionRecyclerView);
        no_transaction_placeholder = rootView.findViewById(R.id.no_transaction_placeholder);
        itemsDataSet = new ArrayList<ItemsTable>();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        long oneMonthEarlierTimestamp = cal.getTimeInMillis();
        itemsDataSet = MainActivity.itemDatabase.itemDAO().listItems(groupId, oneMonthEarlierTimestamp);
        if(itemsDataSet.isEmpty())
        {
            no_transaction_placeholder.setVisibility(View.VISIBLE);
            viewTransactionRecyclerView.setVisibility(View.INVISIBLE);
        }
        else
        {
            layoutManager = new LinearLayoutManager(getActivity().getApplication());
            viewTransactionRecyclerView.setHasFixedSize(true);
            adapter = new ViewTransactionAdapter(itemsDataSet);
            viewTransactionRecyclerView.setLayoutManager(layoutManager);
            viewTransactionRecyclerView.setAdapter(adapter);
        }
        return rootView;
    }

}
