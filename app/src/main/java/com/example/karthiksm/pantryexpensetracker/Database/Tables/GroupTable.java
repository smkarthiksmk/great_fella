package com.example.karthiksm.pantryexpensetracker.Database.Tables;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity (tableName = "group_table")
public class GroupTable {

    @PrimaryKey (autoGenerate = true)
    private int id;
    @ColumnInfo (name = "groupName")
    private String groupName;
    @ColumnInfo (name = "groupMembers")
    private String groupMembers;
    @ColumnInfo (name = "profileName")
    private String profileName;
    @ColumnInfo (name = "amount")
    private int amount;
    @ColumnInfo (name = "color")
    private int color;
    @ColumnInfo (name = "lastAccountActivity")
    private long lastAccountActivity;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(String groupMembers) {
        this.groupMembers = groupMembers;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public long getLastAccountActivity() {
        return lastAccountActivity;
    }

    public void setLastAccountActivity(long lastAccountActivity) {
        this.lastAccountActivity = lastAccountActivity;
    }
}
