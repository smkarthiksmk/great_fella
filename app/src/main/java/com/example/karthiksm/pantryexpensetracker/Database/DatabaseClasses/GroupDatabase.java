package com.example.karthiksm.pantryexpensetracker.Database.DatabaseClasses;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.karthiksm.pantryexpensetracker.Database.DAO.GroupDAO;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.GroupTable;

@Database(entities = GroupTable.class, version = 1)
public abstract class GroupDatabase extends RoomDatabase {
    public abstract GroupDAO groupDAO();
}

