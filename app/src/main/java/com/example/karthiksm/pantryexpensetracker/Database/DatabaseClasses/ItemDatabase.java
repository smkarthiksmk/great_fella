package com.example.karthiksm.pantryexpensetracker.Database.DatabaseClasses;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.example.karthiksm.pantryexpensetracker.Database.DAO.GroupDAO;
import com.example.karthiksm.pantryexpensetracker.Database.DAO.ItemDAO;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.ItemsTable;

@Database(entities = ItemsTable.class , version = 1)
public abstract class ItemDatabase extends RoomDatabase {
    public abstract ItemDAO itemDAO();
}
