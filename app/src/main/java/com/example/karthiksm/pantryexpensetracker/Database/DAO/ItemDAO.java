package com.example.karthiksm.pantryexpensetracker.Database.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.karthiksm.pantryexpensetracker.Database.Tables.ItemsTable;

import java.util.List;

@Dao
public interface ItemDAO {

    @Insert
    void insertItem(ItemsTable itemsTable);

    @Query("Select * from item_table WHERE groupId  LIKE :groupId AND date >=:oneMonthEarlierTimestamp")
    List<ItemsTable> listItems(int groupId, long oneMonthEarlierTimestamp);

    @Query("Delete from item_table WHERE groupId  LIKE :groupId")
    void deleteItems(int groupId);
}
