package com.example.karthiksm.pantryexpensetracker.Constants;

public class ApplicationConstants {

    public static String groupCreationSuccessful = "Group has been created successfully.";
    public static String groupUpdateSuccessful = "Group details have been updated successfully.";
    public static String groupDeleteSuccessful = "Group has been deleted successfully.";
    public static String transactionSuccessful = "Transaction has been recorded successfully.";
    public static String paymentSuccessful = "Payment has been recorded successfully.";
    public static String requiredFields = "Please fill in all the required fields.";
    public static String validData = "Please enter valid data.";
    public static String somethingWentWrong = "Oops! Something went wrong. Please try again later.";

}
