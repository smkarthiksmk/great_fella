package com.example.karthiksm.pantryexpensetracker;

import android.Manifest;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.example.karthiksm.pantryexpensetracker.Database.DatabaseClasses.GroupDatabase;
import com.example.karthiksm.pantryexpensetracker.Database.DatabaseClasses.ItemDatabase;
import com.example.karthiksm.pantryexpensetracker.Database.DatabaseClasses.PaymentDatabase;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.PaymentsTable;
import com.example.karthiksm.pantryexpensetracker.Fragments.HomeFragment;
import com.example.karthiksm.pantryexpensetracker.Fragments.PaymentsFragment;
import com.example.karthiksm.pantryexpensetracker.Fragments.UserManagementFragment;

public class MainActivity extends AppCompatActivity {

    private HomeFragment homeFragment;
    private UserManagementFragment userManagementFragment;
    private PaymentsFragment paymentsFragment;
    public static GroupDatabase groupDatabase;
    public static ItemDatabase itemDatabase;
    public static PaymentDatabase paymentDatabase;
    public static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    doPermAudio();
                }
            }
        });
        setContentView(R.layout.activity_main);

        homeFragment = new HomeFragment();
        userManagementFragment = new UserManagementFragment();
        paymentsFragment = new PaymentsFragment();
        groupDatabase = Room.databaseBuilder(this, GroupDatabase.class, "group_table").allowMainThreadQueries().build();
        itemDatabase = Room.databaseBuilder(this, ItemDatabase.class, "item_table").allowMainThreadQueries().build();
        paymentDatabase = Room.databaseBuilder(this, PaymentDatabase.class, "payments_table").allowMainThreadQueries().build();

        setFragment(homeFragment);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            System.out.println("line 26 " + item.getItemId());
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    setFragment(homeFragment);
                    return true;

                case R.id.navigation_payments:
                    setFragment(paymentsFragment);
                    return true;

                case R.id.navigation_userManagement:
                    setFragment(userManagementFragment);
                    return true;

            }
            return false;
        }

    };

    private void setFragment(Fragment fragment) {
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content, fragment).addToBackStack(null).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                v.clearFocus();
                if (!outRect.contains((int) event.getRawX(), (int) event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }
    void doPermAudio()
    {
        int MY_PERMISSIONS_RECORD_AUDIO = 1;
        MainActivity thisActivity = this;

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(thisActivity,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    MY_PERMISSIONS_RECORD_AUDIO);
        }
    }

}
