package com.example.karthiksm.pantryexpensetracker.Utilities;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class Utility {
    public static boolean isValid(String value)
    {
        if(value.isEmpty() && value.equals(null))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    public static long getTimeStamp()
    {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return timestamp.getTime();
    }
    public static String getFormattedDate(long timeStamp, String pattern)
    {
        DateFormat dateFormat = new SimpleDateFormat(pattern);

        return dateFormat.format(timeStamp);
    }
}
