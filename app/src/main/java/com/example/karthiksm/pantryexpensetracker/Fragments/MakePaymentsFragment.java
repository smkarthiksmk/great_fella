package com.example.karthiksm.pantryexpensetracker.Fragments;


import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.karthiksm.pantryexpensetracker.Constants.ApplicationConstants;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.GroupTable;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.PaymentsTable;
import com.example.karthiksm.pantryexpensetracker.MainActivity;
import com.example.karthiksm.pantryexpensetracker.R;
import com.example.karthiksm.pantryexpensetracker.Utilities.Utility;

/**
 * A simple {@link Fragment} subclass.
 */
public class MakePaymentsFragment extends Fragment {

    private TextView profilePicture;
    private TextView groupName;
    private TextView pendingAmount;
    private Button paymentLogs;
    private Button makePayment;
    private EditText paymentAmount;

    private int groupId;

    public MakePaymentsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView =  inflater.inflate(R.layout.fragment_make_payments, container, false);
        profilePicture =rootView.findViewById(R.id.profilePicture);
        groupName =rootView.findViewById(R.id.groupName);
        pendingAmount =rootView.findViewById(R.id.pendingAmount);
        paymentLogs =rootView.findViewById(R.id.viewPayments);
        makePayment =rootView.findViewById(R.id.makePayment);
        paymentAmount =rootView.findViewById(R.id.paymentAmount);

        groupId = getArguments().getInt("id");
        profilePicture.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        profilePicture.setBackground(generateShape(profilePicture.getMeasuredWidth(), getArguments().getInt("profileColor")));
        profilePicture.setText(getArguments().getString("profileName"));
        groupName.setText(getArguments().getString("groupName"));
        pendingAmount.setText("₹ " + getArguments().getInt("amount"));

        makePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(paymentAmount.getText().toString().isEmpty())
                {
                    Snackbar.make(rootView, ApplicationConstants.requiredFields, Snackbar.LENGTH_LONG).show();
                }
                else if(Integer.parseInt(paymentAmount.getText().toString())>getArguments().getInt("amount")||Integer.parseInt(paymentAmount.getText().toString())==0)
                {
                    Snackbar.make(rootView, ApplicationConstants.validData, Snackbar.LENGTH_LONG).show();
                }
                else
                {
                    try {
                        PaymentsTable paymentsTable = new PaymentsTable();
                        paymentsTable.setGroupId(groupId);
                        paymentsTable.setAmountPaid(Integer.parseInt(paymentAmount.getText().toString()));
                        paymentsTable.setDate(Utility.getTimeStamp());
                        paymentsTable.setOutstandingBalance(getArguments().getInt("amount"));
                        MainActivity.paymentDatabase.paymentDAO().insertPayment(paymentsTable);

                        int tempBalanceCalculation = getArguments().getInt("amount") - Integer.parseInt(paymentAmount.getText().toString());
                        GroupTable groupTable = new GroupTable();
                        groupTable.setId(getArguments().getInt("id"));
                        groupTable.setProfileName(getArguments().getString("profileName"));
                        groupTable.setColor(getArguments().getInt("profileColor"));
                        groupTable.setGroupName(getArguments().getString("groupName"));
                        groupTable.setGroupMembers(getArguments().getString("groupMembers"));
                        groupTable.setLastAccountActivity(Utility.getTimeStamp());
                        groupTable.setAmount(tempBalanceCalculation);
                        MainActivity.groupDatabase.groupDAO().updateGroup(groupTable);
                        Snackbar.make(rootView, ApplicationConstants.paymentSuccessful, Snackbar.LENGTH_LONG).show();
                        MainActivity.fragmentManager.beginTransaction().replace(R.id.content, new PaymentsFragment()).addToBackStack(null).commit();
                    }
                    catch (Exception e)
                    {
                        Snackbar.make(rootView, ApplicationConstants.somethingWentWrong, Snackbar.LENGTH_LONG).show();
                        MainActivity.fragmentManager.beginTransaction().replace(R.id.content, new PaymentsFragment()).addToBackStack(null).commit();
                    }
                }
            }
        });

        paymentLogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ViewPaymentLogsFragment viewPaymentLogsFragment =new ViewPaymentLogsFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("id",groupId);
                viewPaymentLogsFragment.setArguments(bundle);
                MainActivity.fragmentManager.beginTransaction().replace(R.id.content, viewPaymentLogsFragment).addToBackStack(null).commit();
            }
        });
        return rootView;
    }
    public GradientDrawable generateShape(int imageWidth, int color) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setColor(color);
        shape.setCornerRadius(imageWidth);
        return shape;
    }
}
