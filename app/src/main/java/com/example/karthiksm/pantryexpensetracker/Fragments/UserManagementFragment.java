package com.example.karthiksm.pantryexpensetracker.Fragments;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.karthiksm.pantryexpensetracker.Adapter.UserManagementAdapter;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.GroupTable;
import com.example.karthiksm.pantryexpensetracker.MainActivity;
import com.example.karthiksm.pantryexpensetracker.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserManagementFragment extends Fragment implements View.OnClickListener {

    private FloatingActionButton addUser;
    private AddUserFragment addUserFragment;

    private RecyclerView userManagementRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;
    private List<GroupTable> userManagementDataset;
    private TextView no_group_placeholder;

    public UserManagementFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user_management, container, false);
        addUser = rootView.findViewById(R.id.addUser);
        addUser.setOnClickListener(this);
        userManagementRecyclerView = rootView.findViewById(R.id.paymentsRecyclerView);
        no_group_placeholder = rootView.findViewById(R.id.no_group_placeholder);


        userManagementDataset = new ArrayList<GroupTable>();
        userManagementDataset = MainActivity.groupDatabase.groupDAO().listGroups();
        if(userManagementDataset.isEmpty())
        {
            no_group_placeholder.setVisibility(View.VISIBLE);
            userManagementRecyclerView.setVisibility(View.INVISIBLE);
        }
        else
        {
        layoutManager = new LinearLayoutManager(getActivity().getApplication());
        userManagementRecyclerView.setHasFixedSize(true);
        adapter = new UserManagementAdapter(userManagementDataset);
        userManagementRecyclerView.setLayoutManager(layoutManager);
        userManagementRecyclerView.setAdapter(adapter);
        }

        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case  R.id.addUser:
                addUserFragment = new AddUserFragment();
                setFragment(addUserFragment);
                break;

            default:
        }
    }

    //TODO: Make transition to another fragment
    private void setFragment(Fragment fragment) {
        MainActivity.fragmentManager .beginTransaction().replace(R.id.content, addUserFragment).addToBackStack(null).commit();
    }
}
