package com.example.karthiksm.pantryexpensetracker.Database.DAO;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.karthiksm.pantryexpensetracker.Database.Tables.GroupTable;

import java.util.List;

@Dao
public interface GroupDAO {

    @Insert
    void addGroup(GroupTable groupTable);

    @Query("Select * from group_table")
    List<GroupTable> listGroups();

    @Update
    void updateGroup(GroupTable groupTable);

    @Query("Delete from group_table WHERE id  LIKE :groupId")
    void deleteGroup(int groupId);
}
