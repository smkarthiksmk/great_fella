package com.example.karthiksm.pantryexpensetracker.Fragments;


import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.karthiksm.pantryexpensetracker.Constants.ApplicationConstants;
import com.example.karthiksm.pantryexpensetracker.Database.Tables.GroupTable;
import com.example.karthiksm.pantryexpensetracker.MainActivity;
import com.example.karthiksm.pantryexpensetracker.R;
import com.example.karthiksm.pantryexpensetracker.Utilities.Utility;

import java.util.ArrayList;
import java.util.Random;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserDetailsFragment extends Fragment {

    private EditText groupName;
    private TextView groupProfilePicture;
    private Button addMember;
    private EditText memberName;
    private TagContainerLayout memberNamesGroup;
    private Button updateGroup;
    private TextView membersCount;
    private Button deleteGroup;


    private int color;
    private ArrayList<String> memberNames;
    Random randomNumber;
    private Button viewTransaction;

    private int groupId;

    public UserDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View rootView = inflater.inflate(R.layout.fragment_user_details, container, false);
        //setting Id for resources
        viewTransaction = rootView.findViewById(R.id.viewTransaction);
        groupName = rootView.findViewById(R.id.groupName);
        groupProfilePicture = rootView.findViewById(R.id.groupProfilePicture);
        addMember = rootView.findViewById(R.id.addMember);
        memberName = rootView.findViewById(R.id.memberName);
        memberNamesGroup = rootView.findViewById(R.id.memberNamesGroup);
        updateGroup = rootView.findViewById(R.id.createGroup);
        membersCount = rootView.findViewById(R.id.membersCount);
        deleteGroup = rootView.findViewById(R.id.deleteGroup);

        groupId = getArguments().getInt("id");
        groupName.setText(getArguments().getString("groupName"));
        groupProfilePicture.setText(getArguments().getString("profileName"));
        memberNames = new ArrayList<String>();
        String[] splitString = getArguments().getString("groupMembers").split(", ");
        for (int i = 0; i < splitString.length; i++) {
            memberNames.add(splitString[i]);
        }
        membersCount.setText("(" + memberNames.size() + ")");
        memberNamesGroup.setTags(memberNames);

        viewTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewTransactionsFragment viewTransactionsFragment = new ViewTransactionsFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("id", groupId);
                viewTransactionsFragment.setArguments(bundle);
                MainActivity.fragmentManager.beginTransaction().replace(R.id.content, viewTransactionsFragment).addToBackStack(null).commit();
            }
        });
        //Initializers

        randomNumber = new Random();
        color = Color.argb(255, randomNumber.nextInt(256), randomNumber.nextInt(256), randomNumber.nextInt(256));

        //random function to generate background color for the profile picture
        groupProfilePicture.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        groupProfilePicture.setBackground(generateShape(groupProfilePicture.getMeasuredWidth()));

        //listeners for group name
        groupName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                try {
                    groupProfilePicture.setBackground(generateShape(groupProfilePicture.getWidth()));
                    if (groupName.getText().length() != 0) {
                        String tempGroupName = (groupName.getText()).toString();
                        String splittedString[] = tempGroupName.split("\\s+");
                        if (splittedString.length == 1) {
                            String tempString = splittedString[0].toUpperCase();
                            tempString = String.valueOf(tempString.charAt(0));
                            groupProfilePicture.setText(tempString);
                        } else {
                            String tempStringOne = splittedString[0].toUpperCase();
                            String tempStringTwo = splittedString[1].toUpperCase();
                            tempStringOne = String.valueOf(tempStringOne.charAt(0));
                            tempStringTwo = String.valueOf(tempStringTwo.charAt(0));
                            groupProfilePicture.setText(tempStringOne + tempStringTwo);
                        }
                    } else {
                        groupProfilePicture.setText("NA");
                    }
                } catch (Exception exception) {
                    System.out.println(exception);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        //on click method of add member
        addMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!memberName.getText().toString().isEmpty()) {
                    memberNames.add(memberName.getText().toString());
                    memberName.setText("");
                    memberNamesGroup.setTags(memberNames);
                    membersCount.setText("(" + memberNames.size() + ")");
                }
            }
        });

        //onclick of tag elements
        memberNamesGroup.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {

            }

            @Override
            public void onTagLongClick(int position, String text) {

            }

            @Override
            public void onTagCrossClick(int position) {
                memberNames.remove(position);
                memberNamesGroup.setTags(memberNames);
                membersCount.setText("(" + memberNames.size() + ")");
            }
        });

        //onclick of create group
        updateGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (Utility.isValid(groupName.toString()) && !memberNames.isEmpty()) {
                        String listString = memberNames.get(0);
                        for (int i = 1; i < memberNames.size(); i++) {
                            listString = listString + ", " + memberNames.get(i);
                        }
                        GroupTable groupTable = new GroupTable();
                        groupTable.setId(groupId);
                        groupTable.setGroupName(groupName.getText().toString());
                        groupTable.setGroupMembers(listString);
                        groupTable.setColor(color);
                        groupTable.setProfileName(groupProfilePicture.getText().toString());
                        groupTable.setLastAccountActivity(Utility.getTimeStamp());
                        groupTable.setAmount(getArguments().getInt("amount"));
                        MainActivity.groupDatabase.groupDAO().updateGroup(groupTable);
                        Snackbar.make(rootView, ApplicationConstants.groupUpdateSuccessful, Snackbar.LENGTH_LONG).show();

                    } else {
                        Snackbar.make(rootView, ApplicationConstants.requiredFields, Snackbar.LENGTH_LONG).show();
                    }
                } catch (Exception exception) {
                    System.out.println(exception);
                    Snackbar.make(rootView, ApplicationConstants.somethingWentWrong, Snackbar.LENGTH_LONG).show();
                }

            }
        });

        //onclick of create group
        deleteGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    MainActivity.paymentDatabase.paymentDAO().deletePayments(groupId);
                    MainActivity.itemDatabase.itemDAO().deleteItems(groupId);
                    MainActivity.groupDatabase.groupDAO().deleteGroup(groupId);
                    MainActivity.fragmentManager.beginTransaction().replace(R.id.content, new UserManagementFragment()).addToBackStack(null).commit();
                    Snackbar.make(rootView, ApplicationConstants.groupDeleteSuccessful, Snackbar.LENGTH_LONG).show();
                } catch (Exception exception) {
                    Snackbar.make(rootView, ApplicationConstants.somethingWentWrong, Snackbar.LENGTH_LONG).show();
                }

            }
        });

        return rootView;
    }

    public GradientDrawable generateShape(int imageWidth) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setColor(color);
        shape.setCornerRadius(imageWidth);
        return shape;
    }

}
